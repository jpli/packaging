#!/bin/bash

proj_path="$1"
shift

proj_name=`basename $proj_path .py`
work_dir=`dirname $proj_path`

supervisord_conf=/tmp/supervisord.conf
schedule_db=/tmp/celerybeat-schedule

cd $work_dir || exit 1

rm -f /tmp/celery* /tmp/supervisord*

cat >$supervisord_conf <<EOF
[supervisord]
nodaemon=true
logfile=/tmp/supervisord.log
pidfile=/tmp/supervisord.pid

[program:celery-worker]
command=/usr/bin/celery worker --without-mingle --without-gossip --without-heartbeat -A $proj_name -l info -c 1 --pidfile=/tmp/celery-worker.pid $@
EOF

exec /usr/bin/supervisord -c $supervisord_conf
