#!/bin/bash

trap quit SIGTERM

quit() {
    echo "Stopping nginx ..."
    kill -SIGQUIT `cat $NGINX_PID_FILE`

    echo "Stopping fcgiwrap ..."
    kill -SIGTERM `cat $FCGIWRAP_PID_FILE`

    echo "Quitting ..."
    exit
}

CGIT_REPOS="$1"

FCGIWRAP_PID_FILE=/tmp/fcgiwrap.pid
NGINX_PID_FILE=`grep ^pid /etc/nginx/nginx.conf | awk '{print $2}' | sed 's/;$//'`

if [ -r "$CGIT_REPOS" ]; then
    cp "$CGIT_REPOS" /etc/cgitrepos
fi

spawn-fcgi -U nginx -G nginx -s /var/run/fcgiwrap.socket -P $FCGIWRAP_PID_FILE /usr/sbin/fcgiwrap
nginx

while true; do
    sleep 1
done
